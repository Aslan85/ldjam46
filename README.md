# ldjam46

## Ludum Dare 46

Game created by Joss for the challenge [Ludum Dare 46](https://ldjam.com/events/ludum-dare/46)

The theme is 「Keep it alive」.

I keep a blog for this event where I'm writting my [progress](https://ldjam.com/users/aslan85/feed).


## Tools

I'm using the game engine [Haxeflixel](https://haxeflixel.com/), code with [Visual Studio Code](https://code.visualstudio.com/) and write the sprites with [Aseprite](https://www.aseprite.org/)


## Concept

It's a game cut in two parts.
Fist one will be a simple platform game where the player must get a lot of coins.
Second part will be a narrative game where the player must justify why he is going out there.

The "it" of "Keep it alive" concerns his dreams, projects, future.


## About me

I'm Josselin, a French Programmer working at Tokyo.
You can reach me on [mastodon](https://mastodon.gamedev.place/@Joss) or [twitter](https://twitter.com/josselinjp).
I also have a [personnal website](http://chickenmelody.com/).
